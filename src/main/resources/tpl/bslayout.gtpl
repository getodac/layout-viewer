xmlDeclaration()
html {
    head {
        meta(charset:"utf-8")
        newLine()
        title('Layout Viewer')
        newLine()
        script(src:'https://code.jquery.com/jquery-2.2.4.min.js') {}
        newLine()
        link(rel: 'stylesheet', href: 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css', crossorigin: 'anonymous') {}
        style {
            include unescaped: 'tpl/style.css'
        }
        newLine()
    }
    body {
        sections.each { section ->
            div(class: 'border border-primary section') {
                span "Section [${section.id}]"
                section.fieldSets.each { fs ->
                    div(class: 'border border-secondary fieldset') {
                        span "Fieldset [${fs.id}]"
                        div(class: 'row') {
                            printElements(fs.elements)
                        }
                    }
                }
            }
        }
    }
}



def printElements(def items) {
    items.each { element ->
        div(class: "border border-info ${element.span} element") {
            span {
                b(element.id)
                yeld '[' + element.displayOrder + ','
                yeld element.type
                if (element.text) {
                    yeld ', ' + element.text
                }
                if (element.field) {
                    yeld ', f=' + element.field.id
                    yeld ', ft=' + element.field.name
                }
                if (element.expression) {
                    yeld ', exp=' + element.expression
                }
                yeld ']'
            }
            div(class: "row") {
                printElements(element.elements)
            }
        }
    }
}