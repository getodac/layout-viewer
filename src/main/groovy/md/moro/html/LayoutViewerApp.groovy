package md.moro.html

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.globaltradecorp.privateapi.cms.dto.SectionDTO
import javafx.application.Application
import javafx.event.ActionEvent
import javafx.event.EventHandler
import javafx.geometry.Rectangle2D
import javafx.scene.Scene
import javafx.scene.control.Alert
import javafx.scene.control.Menu
import javafx.scene.control.MenuBar
import javafx.scene.control.MenuItem
import javafx.scene.control.TextInputDialog
import javafx.scene.layout.BorderPane
import javafx.scene.layout.HBox
import javafx.scene.layout.StackPane
import javafx.scene.web.WebView
import javafx.stage.FileChooser
import javafx.stage.Screen
import javafx.stage.Stage

/**
 * @author Ion Morosanu (ion.morosanu@codefactorygroup.com), 9/24/19
 */
class LayoutViewerApp extends Application {
    private WebView webView = new WebView()
    private WebExporter exporter = new WebExporter()

    void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Layout Viewer")

        Screen screen = Screen.getPrimary()
        Rectangle2D bounds = screen.getVisualBounds()

        //webView.getEngine().loadContent('hello world', 'text/html')
        //webView.getEngine().loadContent(getContent(), 'text/html')

        FileChooser fileChooser = new FileChooser();
        MenuBar menuBar = new MenuBar()
        Menu menu1 = new Menu("File")
        MenuItem loadJsonFromText = new MenuItem('Load json')
        MenuItem loadJsonFromFile = new MenuItem('Load json from file')
        loadJsonFromFile.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            void handle(ActionEvent event) {
                File selectedFile = fileChooser.showOpenDialog(primaryStage)
                try {
                    webView.getEngine().loadContent(getContent(selectedFile.text), 'text/html')
                } catch (Exception e) {
                    Alert alert = new Alert(Alert.AlertType.WARNING, e.getMessage())
                    alert.showAndWait()
                }
            }
        })
        loadJsonFromText.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            void handle(ActionEvent event) {
                TextInputDialog dialog = new TextInputDialog()
                dialog.setHeaderText("JSON Input")
                dialog.setContentText("Put your JSON here")
                def text = dialog.showAndWait()
                if (text.isPresent()) {
                    try {
                        webView.getEngine().loadContent(getContent(text.get()), 'text/html')
                    } catch (Exception e) {
                        Alert alert = new Alert(Alert.AlertType.WARNING, e.getMessage())
                        alert.showAndWait()
                    }
                } else {
                    Alert alert = new Alert(Alert.AlertType.INFORMATION, 'No json was provided.')
                    alert.showAndWait()
                }
            }
        })
        menu1.getItems().addAll(loadJsonFromText, loadJsonFromFile)
        menuBar.getMenus().addAll(menu1)
        HBox menuBox = new HBox(menuBar)
        StackPane webPane = new StackPane(webView)

        BorderPane root = new BorderPane()
        root.setTop(menuBox)
        root.setCenter(webPane)



        primaryStage.setX(bounds.getMinX())
        primaryStage.setY(bounds.getMinY())
        primaryStage.setWidth(bounds.getWidth())
        primaryStage.setHeight(bounds.getHeight())
        Scene scene = new Scene(root)
        print "x = ${bounds.getMinX()}, y = ${bounds.getMinY()}, w = ${bounds.getWidth()}, h = ${bounds.getHeight()}"

        primaryStage.setScene(scene)
        primaryStage.show()
    }

    private String getContent(String text) {
        ObjectMapper om = new ObjectMapper()
        TypeReference<List<SectionDTO>> ref =new TypeReference<List<SectionDTO>>(){}
        List<SectionDTO> sections = om.readValue(text, ref)
        return exporter.export('tpl/bslayout.gtpl', [sections: sections]).toString()
    }
}
