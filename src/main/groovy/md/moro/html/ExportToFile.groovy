package md.moro.html

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.globaltradecorp.privateapi.cms.dto.SectionDTO

/**
 * @author Ion Morosanu (ion.morosanu@codefactorygroup.com), 9/24/19
 */
class ExportToFile {
    static void main(String[] args) {
        new File('test.html') << getContent(new WebExporter())
    }

    private static String getContent(WebExporter exporter) {
        ObjectMapper om = new ObjectMapper()
        TypeReference<List<SectionDTO>> ref =new TypeReference<List<SectionDTO>>(){}
        InputStream stream = getClass().getResourceAsStream('/sections.json')
        List<SectionDTO> sections = om.readValue(stream, ref)
        return exporter.export('tpl/bslayout.gtpl', [sections: sections]).toString()
    }

}
