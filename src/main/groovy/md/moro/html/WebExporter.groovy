package md.moro.html

import groovy.text.markup.MarkupTemplateEngine
import groovy.text.markup.TemplateConfiguration

/**
 * @author Ion Morosanu (ion.morosanu@codefactorygroup.com), 4/22/19
 */
class WebExporter {
    Writable export(String templatePath, Map model) {
        TemplateConfiguration config = new TemplateConfiguration()
        config.setAutoNewLine(true)
        config.setAutoIndent(true)
        config.setDeclarationEncoding('UTF-8')
        MarkupTemplateEngine engine = new MarkupTemplateEngine(config)
        def template = engine.createTemplateByPath(templatePath)
        return template.make(model)
    }
}
